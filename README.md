# pam_afs_session

* pam-afs-session is a PAM module intended for use with a Kerberos v5 PAM module
to obtain an AFS PAG (Process Authentication Group) and AFS tokens on login. It
puts every new session in a PAG regardless of whether it was authenticated with
Kerberos and runs a configurable external program to obtain tokens.
