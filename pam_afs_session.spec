Name: pam_afs_session
Summary: AFS PAG and AFS tokens on login
Version: 2.6
Release: 3%{?dist}
License: MIT
Group: System Environment/Base
URL: http://www.eyrie.org/~eagle/software/pam-afs-session/
Source: http://archives.eyrie.org/software/afs/pam-afs-session-%{version}.tar.gz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pam-devel%{?_isa}
BuildRequires: krb5-devel%{?_isa}
BuildRequires: gcc
%description
pam-afs-session is a PAM module intended for use with a Kerberos v5 PAM module
to obtain an AFS PAG (Process Authentication Group) and AFS tokens on login. It
puts every new session in a PAG regardless of whether it was authenticated with
Kerberos and runs a configurable external program to obtain tokens.

%define pamdir /%{_lib}/security

%prep
%setup -q -n pam-afs-session-%{version}
# remove non-redhat examples
find examples -mindepth 1 -maxdepth 1 -not -name "redhat" -exec rm -rf {} ';'

%build
%configure --libdir=/%{_lib} --with-aklog=%{_bindir}/aklog
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{pamdir}/*.la

%clean
rm -rf $RPM_BUILD_ROOT

%files
%{!?_licensedir:%global license %%doc}
%license LICENSE
%doc NEWS README TODO examples
%{pamdir}/pam_afs_session.so
%{_mandir}/man5/pam_afs_session.5.gz

%changelog
* Wed Jan 22 2020 Ben Morrice <ben.morrice@cern.ch> - 2.6-3
- rebuild for el8

* Wed Feb 15 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.6-2
- rebuild for SLC5/6 and CC7

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 2.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jan 28 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.6-1
- Update to pam_afs_session 2.6
- Remove unneeded %%defattr
- Use %%license macro

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Fri Jun 06 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Feb 07 2012 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.5-3
- Remove pam_krb5 requirement

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Aug 2 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.5-1
- New upstream release.

* Thu Jun 9 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.4-1
- New upstream release.

* Thu Mar 17 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.2-4
- configure --with-aklog. Your aklog binary better follow the FHS.

* Thu Mar 17 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.2-3
- add dist tag.

* Sun Mar 06 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.2-2
- modify source's examples folder in prep stage

* Sat Mar 05 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.2-1
- New upstream release.
- Only include redhat docs.

* Wed Mar 02 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.1-3
- Rename pam-afs-session to pam_afs_session

* Tue Feb 22 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.1-2
- Add %{?_isa} on the BuildRequires -devel packages

* Thu Feb 17 2011 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.1-1
- Update to 2.1
- rpmlint fixes

* Fri Sep 10 2010 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.7-1
- basic spec obtained from http://crcmedia.hpcc.nd.edu/wiki/index.php/Automatic_CRC/ND_AFS_cell_setup
- switch out Russ Allbery's pam-krb5 for RedHat's pam_krb5 in Requires
